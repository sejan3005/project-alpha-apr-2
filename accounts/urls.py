from django.urls import path
from .views import LogIn, LogOut, CreateUser


urlpatterns = [
    path("login/", LogIn, name="login"),
    path("logout/", LogOut, name="logout"),
    path("signup/", CreateUser, name="signup"),
]
